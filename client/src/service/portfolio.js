import axios from "axios";
import { BASE_URL } from ".";

export async function getAccountChart() {
    try {
        // const result = await axios.get("data/account.json");
        const id = localStorage.getItem("id");
        const result = await axios.get(`${BASE_URL}/chart?accId=${id}`);
        return result.data;
    } catch (err) {
        return err;
    }
}

export async function getPortfolioPerformanceChart() {
    try {
        const result = await axios.get("data/portfolioPerformance.json");
        return result.data;
    } catch (err) {
        return err;
    }
}

export async function getCompositionProfile() {
    try {
        const id = localStorage.getItem("id");
        const res = await axios.get(`${BASE_URL}/holdings/Donut?accId=${id}`);
        // const res = await axios.get("data/donutChart.json");
        return res.data;
    } catch (err) {
        return err;
    }
}
