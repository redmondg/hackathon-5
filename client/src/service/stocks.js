import axios from "axios";

import { BASE_URL } from ".";

export async function getStocksPurchased() {
    try {
        const id = localStorage.getItem("id");
        const res = await axios.get(`${BASE_URL}/holdings/stocks?accId=${id}`);
        return res.data;
    } catch (err) {
        return err;
    }
}

export async function getData(stock) {
    console.log(stock);
    const data = {
        current: "130",
        previousday: [{ high: "150", low: "120", open: "130", close: "130" }],
        week52: [{ high: "150", low: "120" }],
        volume: [{ today: "1200", avg: "1300" }],
    };
    console.log(data);
    return data;
}

export async function getToday(stock) {
    let res;
    console.log("HERE");
    await axios
        .get(
            `https://65yrtzxkv2.execute-api.ap-southeast-1.amazonaws.com/SinglePrice?symbol=${stock}`
        )
        .then((response) => {
            console.log(response.data);
            res = response.data;
        })
        .catch((error) => {
            console.log(error);
        });
    return res;
}

export async function getHistData(stock, range) {
    console.log(stock, range);
    let res;
    if (range == "daily") {
        await axios
            .get(
                `https://mn7q8pqe6b.execute-api.ap-southeast-1.amazonaws.com/Prod?symbol=${stock}`
            )
            .then((response) => {
                console.log(response.data);
                res = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
    } else if (range == "weekly") {
        await axios
            .get(
                `https://mn7q8pqe6b.execute-api.ap-southeast-1.amazonaws.com/Prod?symbol=${stock}`
            )
            .then((response) => {
                console.log(response.data);
                res = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
    } else {
        console.log('hello');
        await axios
            .get(
                `https://yrvmm9pj4l.execute-api.ap-southeast-1.amazonaws.com/Prod?symbol=${stock}`
            )
            .then((response) => {
                console.log(response.data);
                res = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
    }

    const data = {
        labels: Object.keys(res),
        datasets: [
            {
                label: "",
                data: Object.values(res),
                fill: false,
                backgroundColor: "rgba(202, 43, 81, 1)",
                borderColor: "rgba(202, 43, 81, 1)",
            },
        ],
    };
    return data;
}

export async function buyStock(ticker, quantity) {
    const id = localStorage.getItem("id");
    console.log(ticker, quantity)
    const dets = {
        accId: parseInt(id),
        ticker: ticker,
        quantity: parseInt(quantity),
        actionType: "BUY"
    }
    let alert;
    await axios.post("http://localhost:8080/transaction", dets, {header: {"Content-Type":"application/json"}})
        .then((response) => {
            console.log(response);
            alert = "Buy order is successfully placed";
        }, (error) => {
            console.log(error);
            alert = "Insufficient Cash in account";
        })
    return alert;
}

export async function sellStock(ticker, quantity) {
    console.log(ticker, quantity)
    const id = localStorage.getItem("id");
    const dets = {
        accId: parseInt(id),
        ticker: ticker,
        quantity: parseInt(quantity),
        actionType: "SELL"
    }
    let alert;
    await axios.post("http://localhost:8080/transaction", dets, {header: {"Content-Type":"application/json"}})
        .then((response) => {
            console.log(response.data);
            alert = response.data;
        }, (error) => {
            console.log(error);
            alert = "Insufficient stocks in account";
        })
    return alert;
}
