import axios from "axios";
import { BASE_URL } from ".";

export async function getUserProfile() {
    try {
        const id = localStorage.getItem("id");
        const res = await axios.get(`${BASE_URL}/holdings/Home?accId=${id}`);
        // const res = await axios.get("data/homeData.json");
        return res.data;
    } catch (err) {
        return err;
    }
}

export async function updateDeposit(cash) {
    try {
        const id = localStorage.getItem("id");
        const res = await axios.put(
            `${BASE_URL}/portfolio/cash/accId`,
            null,
            {
                params: {
                    id,
                    cash,
                },
            },
            {
                Headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Access-Control-Allow-Origin": "*",
                },
            }
        );
        console.log(res);
        return true;
    } catch (err) {
        return err;
    }
}

export async function updateWithdrawal(negCash) {
    const cash = -negCash;
    try {
        const id = localStorage.getItem("id");
        const res = await axios.put(
            `${BASE_URL}/portfolio/cash/accId`,
            null,
            {
                params: {
                    id,
                    cash,
                },
            },
            {
                Headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Access-Control-Allow-Origin": "*",
                },
            }
        );
        console.log(res);
        return true;
    } catch (err) {
        return err;
    }
}

export async function getNewsFeed() {
    const res = await axios.get(`${BASE_URL}/news`);
    return res.data;
}
