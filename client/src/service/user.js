import { BASE_URL } from ".";
import axios from "axios";

export async function loginUser(email, password) {
    try {
        const res = await axios.get(
            `${BASE_URL}/account?email=${email}&password=${password}`
        );
        return {
            success: true,
            ...res.data,
        };
    } catch (err) {
        return {
            success: false,
            error: err,
        };
    }
}

export async function registerUser(password, firstName, lastName, email) {
    try {
        await axios.post(`${BASE_URL}/account`, {
            password,
            firstName,
            lastName,
            email,
        });
        return true;
    } catch (err) {
        console.log(err);
        return false;
    }
}
