import axios from "axios";

export async function getTrades() {
    let rows;
    const id = localStorage.getItem("id");
    await axios.get(`http://localhost:8080/transactions?accId=${id}`)
    .then((response => {
        console.log(response.data)
        rows = response.data;
    }))
    return rows;
}

export async function getHoldings() {
    let rows;
    const id = localStorage.getItem("id");
    await axios.get(`http://localhost:8080/holdings/stocks?accId=${id}`)
    .then((response => {
        console.log(response.data)
        rows = response.data;
    }))
    return rows;
}