export const constants = {
    feedInterval:
        process.env.NODE_ENV === "development" ||
        process.env.NODE_ENV === "test"
            ? 5 * 60 * 1000
            : 1 * 60 * 1000,
};
