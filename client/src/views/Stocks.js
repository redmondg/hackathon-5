import React, { useRef, useState } from "react";
import {
    Typography,
    Container,
    Card,
    CardContent,
    Box,
    makeStyles,
    Grid,
    Button,
    TextField,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
} from "@material-ui/core";
import StockCards from "../ui/StockCards";
import StockChart from "../ui/StockChart";
import {
    getData,
    getHistData,
    buyStock,
    sellStock,
    getToday,
} from "../service/stocks";

const useStyles = makeStyles({
    root: {
        maxWidth: 300,
    },
    cardTitle: {
        fontSize: 12,
    },
});

export default function Stocks() {
    const [data, setData] = useState();
    const [histData, setHistData] = useState();
    let tempRange;
    const [range, setRange] = useState();
    const [today, setToday] = useState();
    const stock = useRef("");

    const [openBuy, setOpenBuy] = useState(false);
    const [openSell, setOpenSell] = useState(false);
    const qty = useRef(0);

    const handleRadioChange = (event) => {
        console.log(event.target.value);
        setRange(event.target.value);
    };

    const handleBuyOpen = () => {
        setOpenBuy(true);
    };
    const handleBuyClose = () => {
        setOpenBuy(false);
    };
    const handleSellOpen = () => {
        setOpenSell(true);
    };
    const handleSellClose = () => {
        setOpenSell(false);
    };

    return (
        <div style={{ padding: "5rem" }}>
            <Container>
                <Box
                    height={20}
                    width={40}
                    alignItems="center"
                    justifyContent="center"
                />
                <TextField
                    style={{ width: "80%" }}
                    color="secondary"
                    id="outlined-basic"
                    label="Stock Ticker"
                    size="small"
                    variant="outlined"
                    placeholder="Stock Name"
                    inputRef={stock}
                />
                <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    onClick={async () => {
                        setData(await getData(stock.current.value));
                        setHistData(await getHistData(stock.current.value, range));
                        setToday(await getToday(stock.current.value));
                    }}
                >
                    Get Data
                </Button>
                <FormControl component="fieldset">
                    <RadioGroup
                        row
                        aria-label="quiz"
                        name="quiz"
                        onChange={handleRadioChange}
                    >
                        <FormControlLabel
                            value="daily"
                            control={<Radio />}
                            label="Daily"
                        />
                        <FormControlLabel
                            value="weekly"
                            control={<Radio />}
                            label="Weekly"
                        />
                        <FormControlLabel
                            value="monthly"
                            control={<Radio />}
                            label="Monthly"
                        />
                    </RadioGroup>
                </FormControl>

                <Box
                    display="flex"
                    flexDirection="row"
                    p={1}
                    m={1}
                    bgcolor="background.paper"
                >
                    <Grid item xs={12} sm={6} md={6}>
                        <Card style={{ border: "none", boxShadow: "none" }}>
                            <CardContent align="left">
                                <Typography
                                    align="left"
                                    color="#eabf13"
                                    variant="h6"
                                    gutterBottom
                                >
                                    Current Stock Price
                                </Typography>
                                <Box
                                    height={40}
                                    width={40}
                                    alignItems="center"
                                    justifyContent="center"
                                >
                                    {today === undefined ? (
                                        <Typography>{""}</Typography>
                                    ) : (
                                        <Typography>{today['price']}</Typography>
                                    )}
                                </Box>
                            </CardContent>
                        </Card>
                        <div className="portfolio-chart">
                            {histData === undefined ? (
                                <StockChart data="" />
                            ) : (
                                <StockChart data={histData} />
                            )}
                        </div>
                        <Box
                            height={20}
                            width={40}
                            alignItems="center"
                            justifyContent="center"
                        />
                        <Box
                            height={20}
                            width={40}
                            alignItems="center"
                            justifyContent="center"
                        />
                        <Box
                            height={20}
                            width={40}
                            alignItems="center"
                            justifyContent="center"
                        />
                        <Button
                            variant="contained"
                            color="secondary"
                            size="small"
                            onClick={() => handleBuyOpen()}
                        >
                            BUY
                        </Button>
                        <Dialog
                            open={openBuy}
                            onClose={() => handleBuyClose()}
                            aria-labelledby="form-dialog-title"
                        >
                            <DialogTitle id="form-dialog-title">
                                Buy Stock
                            </DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                    Input the quantity of stock you wish to buy.
                                </DialogContentText>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    color="secondary"
                                    id="qty"
                                    label="Stock Quantity"
                                    fullWidth
                                    inputRef={qty}
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button
                                    onClick={() => handleBuyClose()}
                                    color="secondary"
                                >
                                    Cancel
                                </Button>
                                <Button
                                    onClick={async () => {
                                        const res = await buyStock(
                                            stock.current.value,
                                            qty.current.value,
                                        );
                                        handleBuyClose();
                                        alert(res);
                                    }}
                                    color="secondary"
                                >
                                    BUY
                                </Button>
                            </DialogActions>
                        </Dialog>
                        <Button
                            variant="contained"
                            color="secondary"
                            size="small"
                            onClick={() => handleSellOpen()}
                        >
                            SELL
                        </Button>
                        <Dialog
                            open={openSell}
                            onClose={() => handleSellClose()}
                            aria-labelledby="form-dialog-title"
                        >
                            <DialogTitle id="form-dialog-title">
                                Sell Stock
                            </DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                    Input the quantity of stock you wish to
                                    sell.
                                </DialogContentText>
                                <TextField
                                    color="secondary"
                                    autoFocus
                                    margin="dense"
                                    id="qty"
                                    label="Stock Quantity"
                                    fullWidth
                                    inputRef={qty}
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button
                                    color="secondary"
                                    onClick={() => handleSellClose()}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    color="secondary"
                                    onClick={async () => {
                                        const res = await sellStock(
                                            stock.current.value,
                                            qty.current.value,
                                        );
                                        handleSellClose();
                                        alert(res);
                                    }}
                                >
                                    SELL
                                </Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                    <Box
                        height={20}
                        width={290}
                        alignItems="center"
                        justifyContent="center"
                    />
                    <Grid>
                        {today === undefined ? (
                            <Container>
                                <StockCards
                                    title="Previous Day"
                                    key1="High"
                                    value1=""
                                    key2="Low"
                                    value2=""
                                    key3="Open"
                                    value3=""
                                    key4="Close"
                                    value4=""
                                />
                                <Box
                                    height={20}
                                    width={40}
                                    alignItems="center"
                                    justifyContent="center"
                                />
                                <StockCards
                                    title="52 Week"
                                    key1="High"
                                    value1=""
                                    key2="Low"
                                    value2=""
                                />
                                <Box
                                    height={20}
                                    width={40}
                                    alignItems="center"
                                    justifyContent="center"
                                />
                                <StockCards
                                    title="Volume"
                                    key1="Today"
                                    value1=""
                                    key2="Avg"
                                    value2=""
                                />
                            </Container>
                        ) : (
                            <Container>
                                <StockCards
                                    title="Previous Day"
                                    key1="High"
                                    value1={today['high']}
                                    key2="Low"
                                    value2={today['low']}
                                    key3="Open"
                                    value3={today['open']}
                                    key4="Close"
                                    value4=""
                                />
                                <Box
                                    height={20}
                                    width={40}
                                    alignItems="center"
                                    justifyContent="center"
                                />
                                <StockCards
                                    title="52 Week"
                                    key1="High"
                                    value1={data.week52[0].high}
                                    key2="Low"
                                    value2={data.week52[0].low}
                                />
                                <Box
                                    height={20}
                                    width={40}
                                    alignItems="center"
                                    justifyContent="center"
                                />
                                <StockCards
                                    title="Volume"
                                    key1="Today"
                                    value1={data.volume[0].today}
                                    key2="Avg"
                                    value2={data.volume[0].avg}
                                />
                            </Container>
                        )}
                    </Grid>
                </Box>
            </Container>
        </div>
    );
}
