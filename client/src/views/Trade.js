import React, { useEffect, useState } from "react";
import { Container, Box, Typography } from "@material-ui/core";
import TradesTable from "../ui/TradesTable";
import HoldingsTable from "../ui/HoldingsTable";
import { getHoldings, getTrades } from "../service/trades";

export default function Trade() {
    const [rows, setRows] = useState();
    const [hold, setHold] = useState();

    useEffect(async () => {
        setRows(await getTrades()); 
        setHold(await getHoldings());
        console.log(hold)
    }, []);

    return (
        <div style={{ padding: "5rem" }}>
            <Container>
                {hold === undefined ? (
                    <Typography>{""}</Typography>
                ) : (
                    <Container>
                        <Typography align="left" variant="h5">Current Stock Holdings:</Typography>
                        <HoldingsTable rows={hold} />
                        <Box
                            height={20}
                            width={40}
                            alignItems="center"
                            justifyContent="center"
                        />
                        <Typography align="left" variant="h5">Trade History:</Typography>
                        <TradesTable rows={rows} />
                    
                    </Container>
                )}
            </Container>         
        </div>
    );
}
