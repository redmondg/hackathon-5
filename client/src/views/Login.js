import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useState } from "react";
import { loginUser } from "../service/user";
import { useHistory } from "react-router";
import "./Login.css";
import { Typography, Paper, Divider } from "@material-ui/core";
import Link from "@material-ui/core/Link";

export default function Login() {
    const history = useHistory();

    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    const handleLogin = async () => {
        const res = await loginUser(email, password);
        if (res.success) {
            localStorage.setItem("id", res.accId);
            localStorage.setItem("email", res.email);
            history.push("/home");
        } else {
            alert("Invalid email or password!");
        }
    };

    return (
        <div
            style={{
                width: "100%",
                height: "100%",
                backgroundColor: "#424242",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            <Paper elevation={5}>
                <div className="overall-container">
                    <div
                        style={{
                            padding: "10px",
                            fontVariant: "all-petite-caps",
                            fontWeight: "100",
                        }}
                    >
                        <Typography variant="h2">Sign In</Typography>
                    </div>
                    <Divider />
                    <br />
                    <div className="input-field">
                        <TextField
                            id="outlined-basic"
                            label="Email"
                            variant="outlined"
                            value={email}
                            color="secondary"
                            onChange={(event) => setEmail(event.target.value)}
                        />
                    </div>
                    <div className="input-field">
                        <TextField
                            id="outlined-basic"
                            label="Password"
                            variant="outlined"
                            value={password}
                            color="secondary"
                            type="password"
                            onChange={(event) => {
                                setPassword(event.target.value);
                            }}
                        />
                    </div>
                    <div style={{ margin: "2px" }}>
                        <Link href="/register">
                            Don't have an account? Register here!
                        </Link>
                    </div>
                    <div>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleLogin}
                        >
                            Login
                        </Button>
                    </div>
                </div>
            </Paper>
        </div>
    );
}
