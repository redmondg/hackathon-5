import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useState } from "react";
import { registerUser } from "../service/user";
import { useHistory } from "react-router";
import "./Register.css";
import { Typography, Paper, Divider } from "@material-ui/core";

export default function Register() {
    const history = useHistory();

    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [firstName, setFirstName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [emailError, setEmailError] = useState({ error: false, msg: "" });
    const [passwordError, setPasswordError] = useState({
        error: false,
        msg: "",
    });

    const validateEmail = (_email) => {
        const re =
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(_email);
    };

    const validatePassword = (_password) => {
        const re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
        return re.test(_password);
    };

    const handleRegister = () => {
        // validate data
        if (!email || !password || !firstName || !lastName) {
            alert("Please input all required fields!");
            return;
        }

        if (!validateEmail(email)) {
            setEmailError({
                error: true,
                msg: "Please input valid email address!",
            });
            return;
        } else {
            setEmailError({
                error: false,
                msg: "",
            });
        }

        if (!validatePassword(password)) {
            setPasswordError({
                error: true,
                msg: "Password must contain at least one number, one lowercase, one uppercase letter, and is at least six characters!",
            });
            return;
        } else {
            setPasswordError({
                error: false,
                msg: "",
            });
        }

        const res = registerUser(password, firstName, lastName, email);
        if (res) {
            alert("Registration successful!");
            history.push("/");
        }
    };

    return (
        <div
            style={{
                width: "100%",
                height: "100%",
                backgroundColor: "#424242",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            <Paper elevation={5}>
                <div className="overall-container">
                    <div
                        style={{
                            padding: "10px",
                            fontVariant: "all-petite-caps",
                            fontWeight: "100",
                        }}
                    >
                        <Typography variant="h2">Register</Typography>
                    </div>
                    <Divider />
                    <br />
                    <div className="name-input">
                        <TextField
                            required
                            id="outlined-basic"
                            label="First Name"
                            variant="outlined"
                            color="secondary"
                            value={firstName}
                            onChange={(event) =>
                                setFirstName(event.target.value)
                            }
                        />
                        <TextField
                            required
                            id="outlined-basic"
                            label="Last Name"
                            variant="outlined"
                            color="secondary"
                            value={lastName}
                            onChange={(event) =>
                                setLastName(event.target.value)
                            }
                        />
                    </div>
                    <div className="input-field">
                        <TextField
                            required
                            id="outlined-basic"
                            label="Email"
                            variant="outlined"
                            value={email}
                            error={emailError.error}
                            helperText={emailError.msg}
                            color="secondary"
                            onChange={(event) => setEmail(event.target.value)}
                        />
                    </div>
                    <div className="input-field">
                        <TextField
                            required
                            id="outlined-basic"
                            label="Password"
                            type="password"
                            variant="outlined"
                            value={password}
                            error={passwordError.error}
                            helperText={passwordError.msg}
                            color="secondary"
                            onChange={(event) => {
                                setPassword(event.target.value);
                            }}
                        />
                    </div>
                    <div>
                        <Button
                            variant="contained"
                            color="secondary"
                            onClick={handleRegister}
                        >
                            Register
                        </Button>
                    </div>
                </div>
            </Paper>
        </div>
    );
}
