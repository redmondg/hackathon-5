import "./ScrollingTicker.css";
import Ticker from "react-ticker";
import { useState, useEffect } from "react";
import { getSymbols, getFeed } from "../service/ticker";
import { useHistory } from "react-router";
import { constants } from "../constants";

export default function ScrollingTicker() {
    const history = useHistory();
    const [feed, setFeed] = useState([]);
    const [tickerStyle, setTickerStyle] = useState({});

    useEffect(() => {
        asyncStartFeed();
        setInterval(() => {
            asyncStartFeed();
        }, constants.feedInterval);
    }, []);

    useEffect(() => {
        setTickerStyle(getTickerStyle(history.location.pathname));
    }, [history.location.pathname]);

    history.listen((location) => {
        setTickerStyle(getTickerStyle(location.pathname));
    });

    const asyncStartFeed = async () => {
        const _symbols = await getSymbols();
        let feeds = [];
        for (let i = 0; i < 4; i++) {
            const random = Math.floor(Math.random() * _symbols.length);
            const _feed = await getFeed(_symbols[random]);
            feeds.push(_feed);
        }
        setFeed(feeds);
    };

    const GetTickerFeed = () => {
        return (
            <p
                style={{
                    verticalAlign: "middle",
                    whiteSpace: "nowrap",
                    marginLeft: "3rem",
                }}
            >
                {feed.map((item) =>
                    item.note === "" ? (
                        <span
                            key={item.symbol}
                            style={{ color: "#fff", fontSize: "large" }}
                        >
                            <b>{item.symbol}</b> OPEN: {item.open} CLOSE:{" "}
                            {item.close} HIGH: {item.high} LOW: {item.low}
                        </span>
                    ) : (
                        <span
                            style={{
                                color: "rgba(202, 43, 81, 1)",
                                fontSize: "large",
                            }}
                        >
                            {item.note}
                        </span>
                    )
                )}
            </p>
        );
    };

    const getTickerStyle = (path) => {
        return path === "/" || path === "/register"
            ? {
                  position: "fixed",
                  zIndex: 5,
                  top: 0,
                  left: 0,
                  width: 100 + "%",
                  height: 2.8 + "rem",
                  fontSize: 0.7 + "rem",
                  backgroundColor: "#303030",
                  color: "white",
              }
            : {
                  position: "fixed",
                  zIndex: 5,
                  top: 50,
                  left: 0,
                  width: 100 + "%",
                  height: 2.8 + "rem",
                  fontSize: 0.7 + "rem",
                  backgroundColor: "#303030",
                  color: "white",
              };
    };

    return (
        <div style={tickerStyle}>
            <Ticker>{() => <GetTickerFeed />}</Ticker>
        </div>
    );
}
