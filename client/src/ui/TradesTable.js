import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import PropTypes from "prop-types";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

// function createData(name, qty, status, price, amt) {
//   return { name, qty, status, price, amt };
// }

// const rows = [
//   createData('AAPL', 159, "FILLED", 24, 4.0),
//   createData('IBM', 237, "FILLED", 37, 4.3),
//   createData('SE', 262, "FILLED", 24, 6.0),
// ];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function TradesTable(props) {
  const classes = useStyles();
  const {rows} = props;

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Trades</StyledTableCell>
            <StyledTableCell align="right">Buy/Sell</StyledTableCell>
            <StyledTableCell align="right">Quantity</StyledTableCell>
            <StyledTableCell align="right">Status</StyledTableCell>
            <StyledTableCell align="right">Price</StyledTableCell>
            <StyledTableCell align="right">Time</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.ticker}>
              <StyledTableCell component="th" scope="row">
                {row.ticker}
              </StyledTableCell>
              <StyledTableCell align="right">{row.actionType}</StyledTableCell>
              <StyledTableCell align="right">{row.quantity}</StyledTableCell>
              <StyledTableCell align="right">{row.status}</StyledTableCell>
              <StyledTableCell align="right">{row.execPrice}</StyledTableCell>
              <StyledTableCell align="right">{row.trxDateTime}</StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

TradesTable.propTypes = {
    rows: PropTypes.any,
}