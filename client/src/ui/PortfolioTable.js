import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { getStocksPurchased } from "../service/stocks";
import "./PortfolioTable.css";
import { useState, useEffect } from "react";

export default function PortfolioTable() {
    const [tableData, setTableData] = useState([]);

    useEffect(() => {
        getTable();
    }, []);

    const getTable = async () => {
        let table = await getStocksPurchased();
        setTableData(table);
    };

    return (
        <TableContainer style={{ maxHeight: "25vh" }}>
            <Table stickyHeader size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Stocks Symbol</TableCell>
                        <TableCell align="right">Quantity</TableCell>
                        <TableCell align="right">Total Value</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {tableData.map((row, idx) => (
                        <TableRow key={idx}>
                            <TableCell>{row[0]}</TableCell>
                            <TableCell align="right">{row[1]}</TableCell>
                            <TableCell align="right">{row[2]}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
