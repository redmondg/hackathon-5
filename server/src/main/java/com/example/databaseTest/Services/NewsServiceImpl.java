package com.example.databaseTest.Services;

import com.example.databaseTest.ExternalAPI.NewyorkTimes;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class NewsServiceImpl implements NewsService{

    @Override
    public List<Map<String, String>> getLatestHeadlines() {
        return NewyorkTimes.getNews();
    }
}
